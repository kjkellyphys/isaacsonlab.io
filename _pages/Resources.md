---
layout: page
title: Resources
permalink: /resources/
---

#### Literature
- [arXiv](http://arxiv.org)
- [INSPIRE](https://inspirehep.net)

#### Fermilab
- [Joint Experimental-Theoretical Physics Seminar](http://theory.fnal.gov/jetp/)
- [Theory Seminar](http://theory.fnal.gov/seminars/seminars.html)
- [Colloquium](http://www-ppd.fnal.gov/EPPOffice-w/colloq/colloq.html)

#### Experiments
- [ATLAS](http://atlas.ch/)([public results](https://twiki.cern.ch/twiki/bin/view/AtlasPublic))
- [CMS](http://cms.web.cern.ch/cms/index.html)([public results](https://twiki.cern.ch/twiki/bin/view/CMSPublic/PhysicsResults))

#### Other
- [Particle Data Grouop](http://pdg.lbl.gov)
- [Academic Jobs Online](http://academicjobsonline.org/ajo)
- [American Physics Organization](http://aps.org)
