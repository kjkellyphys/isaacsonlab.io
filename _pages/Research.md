---
layout: page
title: Research
permalink: /research/
---

My research interests are quite broad, but I mainly am focused on QCD collider phenomenology, espcially dealing with the LHC. Below are some details on certain topics of interest that I have worked on. A complete list of publications can be found [here](http://inspirehep.net/search?ln=en&p=exactauthor%3AJoshua.Isaacson.1).

#### Resummation and Parton Showers

- Development of the [ResBos2](https://resbos2.gitlab.io) program for N$${}^3$$LL + NNLO $$q_T$$ resummation calculations.
- Working on including sub-leading color effects into parton shower programs

#### Monte Carlo Event Generators

- Improving phase space integration
- Improving parton showers
- Developing a resummation event generator

#### Higgs Phenomenology

- Resummation in the Higgs sector
- Resummation of Higgs + jet

#### Precision Physics and EW Precision Observables

- $$W$$ Mass measurement
- $$\sin\theta_W$$ measurements
- $$p_T^Z$$ and $$\phi^*_\eta$$ measurements



