(function($) {
    $(function() { 
        $('.nav-sublinks').hover(function(){
            $('ul.nav-dropdown').slideDown('medium');},
            function() {
            $('ul.nav-dropdown').slideUp('medium');
        });
    });

    document.querySelector('#nav-toggle').addEventListener('click',function() {
        this.classList.toggle('active');
    });
    $('#nav-toggle').click(function() {
        $('nav ul').toggle();
    });
})(jQuery);
