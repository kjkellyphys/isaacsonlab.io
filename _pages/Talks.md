---
layout: page
title: Talks
permalink: /talks/
---

#### Recent Talks
- Durham Electroweak Working Group Meeting ([pdf](/assets/Talks/Durham_2019.pdf))
- Argonne Theory Seminar ([pdf](/assets/Talks/ArgonneSeminar2019.pdf))
- Next Steps in Quantum Computing Talk ([pdf](/assets/Talks/FeynmanReductionQC.pdf))
- Monash Theory Seminar ([pdf](/assets/Talks/MonashSeminar2018.pdf))
- FNAL Theory Seminar ([pdf](/assets/Talks/FNALSeminar2018.pdf))
- LoopFest 2018 ([pdf](/assets/Talks/LoopFest2018.pdf))
- Color Reconnnection Workshop ([pdf](/assets/Talks/PSR2018_CR.pdf))
- Parton Showers and Resummation ([pdf](/assets/Talks/PSR2018_ResBos.pdf))
- From ResBos1 to ResBos2 ([pdf](/assets/Talks/ResBos1to2.pdf))
- $$W$$ Mass Working Group Meeting 14 November 2018 ([pdf](/assets/Talks/WMass_111418.pdf))

For more details on dates of talks, and other talks given please see [CV](/cv/)
