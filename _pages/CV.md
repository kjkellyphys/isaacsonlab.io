---
layout: page
title: CV
permalink: /cv/
---

[Download CV](/assets/CV.pdf)

Research Interests
==================

- Perturbative QCD
- Resummation
- Monte Carlo Event Generators
- Higgs Phenomenology
- Precision Physics
- EW Precision Observables

Education
=========

#### **Michigan State University**, East Lansing, MI

Doctor of Philosophy, Physics, Fall 2017
- Thesis Topic: _Precision Resummation for the LHC Era_
- Advisors: C.-P. Yuan and Carl Schmidt

Master of Physics, Summer 2013

#### **Case Western Reserve University**, Cleveland, OH

B.S., Physics, May 2011
- *Cum Laude*

Research Experience
===================

#### **Postdoctoral Researcher**,
October 2017 to present \\
Fermi National Accelerator Laboratory

#### **Research Assistant**,
September 2013 to September 2017 \\
Department of Physics and Astronomy, \\
Michigan State University

Publications
============

<span>**“A Study of the PDF uncertainty on the LHC W-boson mass
measurement”**</span> \\
M. Hussein, J. Isaacson, J. Huston. \\
arXiv:1905.00110 [hep-ph]

<span>**“Quantum Computing as a High School Module”**</span> \\
A. Perry, R. Sun, C. Hughes, J. Isaacson, J. Turner. \\
arXiv:1905.00282 [physics.ed-ph]

<span>**“Ultraheavy resonances at the LHC: beyond the QCD
background”**</span> \\
B. A. Dobrescu, R. M. Harris and J. Isaacson. \\
arXiv:1810.09429 [hep-ph]

<span>**“New method for reducing parton distribution function
uncertainties in the high-mass Drell-Yan spectrum”**</span> \\
C. Willis, R. Brock, D. Hayden, T. J. Hou, J. Isaacson, C. Schmidt and
C. P. Yuan. \\
Phys. Rev. D <span>**99**</span>, no. 5, 054004 (2019)

<span>**“Stochastically sampling color configurations”**</span> \\
J. Isaacson and S. Prestel. \\
Phys. Rev. D <span>**99**</span>, no. 1, 014021 (2019)

<span>**“$$R_K$$ anomalies and simplified limits on $$Z'$$ models at the
LHC”**</span> \\
R. S. Chivukula, J. Isaacson, K. A. Mohan, D. Sengupta and
E. H. Simmons. \\
Phys. Rev. D <span>**96**</span>, no. 7, 075012 (2017)

<span>**“Les Houches 2015: Physics at TeV Colliders Standard Model
Working Group Report”**</span> \\
J. R. Andersen <span>*et al.*</span>. \\
arXiv:1605.04692 [hep-ph]

<span>**“Minimal Dilaton Model and the Diphoton Excess”**</span> \\
B. Agarwal, J. Isaacson and K. A. Mohan. \\
Phys. Rev. D <span>**94**</span>, no. 3, 035027 (2016)

<span>**“Implications of CMS analysis of photon-photon interactions for
photon PDFs”**</span> \\
P. Obul, M. Ababekri, S. Dulat, J. Isaacson, C. Schmidt and C.-P. Yuan. \\
Chin. Phys. C <span>**42**</span>, no. 11, 113101 (2018)

<span>**“Resummation of High Order Corrections in Higgs Boson Plus Jet
Production at the LHC”**</span> \\
P. Sun, J. Isaacson, C.-P. Yuan and F. Yuan. \\
Phys. Lett. B <span>**769**</span>, 57 (2017)

<span>**“Factorization for substructures of boosted Higgs
jets”**</span> \\
J. Isaacson, H. n. Li, Z. Li and C.-P. Yuan. \\
Phys. Lett. B <span>**771**</span>, 619 (2017)

<span>**“Nonperturbative functions for SIDIS and Drell–Yan
processes”**</span> \\
P. Sun, J. Isaacson, C.-P. Yuan and F. Yuan. \\
Int. J. Mod. Phys. A <span>**33**</span>, no. 11, 1841006 (2018) 

Conference Talks and Invited Seminars
=====================================

Talk at Precision EW @ LHC workshop, 2 April 2019,\\
IPPP Durham, United Kingdom,\\
Title: *Resummation calculations and benchmarking based on Resbos2*

Theory Seminar, Argonne National Laboratory, 13 February 2019,\\
Title: *Effects of Subleading Colro on Parton Showers*

LHC EW Precision sub-group meeting, 14 November 2018,\\
Title: *Predictions of W/Z pT with Resbos2*

Theory Seminar, Monash University, 4 October 2018,\\
Title: *Steps Toward a Full Color Parton Shower*

Theory Seminar, Fermi National Accelerator Laboratory, 20 September
2018,\\
Title: *Subleading Colored Parton Showers*

Talk at Next Steps in Quantum Science for HEP, 12 September 2018,\\
Fermi National Accelerator Laboratory,\\
Title: *Quantum Computing for Feynman Integral Reduction*

Talk at Loop Fest 2018, 19 July 2018,\\
Michigan State University, East Lansing, MI,\\
Title: *Full Color Parton Showers*

Talk at Satellite meeting on colour reconnection, 7 June 2018,\\
Department of Theoretical Physics and Astronomy, Lund University, Lund,
Sweden,\\
Title: *Full Color Parton Showers*

Talk at Parton Showers, Event Generators, and Resummation 2018, 4 June
2018,\\
Department of Theoretical Physics and Astronomy, Lund University, Lund,
Sweden,\\
Title: *ResBos2 and Full Color Parton Showers*

Talk at LHC EW Precision Sub-group meeting, 23 May, 2018,\\
Title: *From ResBos1 to ResBos2*

Seminar at Particle Theory Group, University of Buffalo, 7 March 2017,\\
Title: *ResBos2*

Seminar at Particle Theory Group, University of California, December 7,
2016, Irvine, CA,\\
Title: *ResBos2*

Advances in QCD and Applications to Hadron Colliders, October 28, 2016,\\
Argonne National Lab, IL,\\
Title: *ResBos2 for Drell-Yan and Higgs Boson productions*

Parallel Talk at *Phenomenology 2016 Symposium (Pheno 2016)*, May 9-11,
2016,\\
University of Pittsburgh, PA,\\
Title: *QCD Resummation Effects on \(W'\rightarrow tb\)*

New Physics Interpretations at the LHC, May 4, 2016,\\
Argonne National Lab, IL,\\
Title: *Roman or Greek? Distinguishing gg from $$\gamma\gamma$$ Fusion*

Parallel Talk at *Phenomenology 2015 Symposium (Pheno 2015)*, May 4-6,
2015,\\
University of Pittsburgh, PA,\\
Title: *Jet Energy Profiles for Electroweak Bosons*

Teaching Experience
===================

#### Teaching Assistant (Graduate Courses) at Michigan State University
- PHYS851: Quantum Mechanics: Fall 2013

#### Teaching Assistant (Undergraduate Courses) at Michigan State University
- ISP205L: Visions of the Universe Lab: Fall 2011, Spring 2012, Fall 2012,
Spring 2012
- PHYS251/252: Intro Physics Lab: Summer 2012, Summer 2013
- PHYS215: Thermodynamics & Modern Physics: Fall 2013

Service
=======

Organizer: Next steps in Quantum Science for HEP, 12-14 September 2018 \\
Theory Seminar Organizer: August 2018 to present

[Download CV](/assets/CV.pdf)
